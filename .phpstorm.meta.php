<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace PHPSTORM_META{
    expectedArguments(\W7\Validate\Validate::addEvent(), 0, 'event', 'before', 'after');
    expectedArguments(\W7\Validate\Validate::handleEventCallback(), 1, 'before', 'after');
}
