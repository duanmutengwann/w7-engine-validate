<?php

/**
 * WeEngine System
 *
 * (c) We7Team 2021 <https://www.w7.cc>
 *
 * This is not a free software
 * Using it under the license terms
 * visited https://www.w7.cc for more details
 */

namespace W7\Validate;

use Closure;
use Itwmw\Validation\Factory;
use Itwmw\Validation\Support\Str;
use Itwmw\Validation\ValidationData;
use Itwmw\Validation\Support\ValidationException;
use W7\Validate\Exception\ValidateException;
use W7\Validate\Exception\ValidateRuntimeException;
use W7\Validate\Support\Common;
use W7\Validate\Support\Concerns\DefaultInterface;
use W7\Validate\Support\Concerns\FilterInterface;
use W7\Validate\Support\Concerns\MessageProviderInterface;
use W7\Validate\Support\DataAttribute;
use W7\Validate\Support\Event\ValidateEventAbstract;
use W7\Validate\Support\MessageProvider;
use Itwmw\Validation\Support\Collection\Collection;
use W7\Validate\Support\Storage\ValidateConfig;
use W7\Validate\Support\ValidateScene;

class Validate extends RuleManager
{
    /**
     * Global Event Handler
     *
     * @link https://v.neww7.com/en/4/Validate.html#event
     * @var array
     */
    protected $event = [];

    /**
     * All validated fields cannot be empty when present
     *
     * @link https://v.neww7.com/en/4/Validate.html#filled
     * @var bool
     */
    protected $filled = true;

    /**
     * The filter. This can be a global function name, anonymous function, etc.
     *
     * @link https://v.neww7.com/en/4/Validate.html#filter
     * @var array
     */
    protected $filter = [];

    /**
     * Sets the specified property to the specified default value.
     *
     * @link https://v.neww7.com/en/4/Validate.html#default
     * @var array
     */
    protected $default = [];

    /**
     * Event Priority
     *
     * @var bool
     */
    private $eventPriority = true;

    /**
     * Events to be processed for this validate
     *
     * @var array
     */
    private $events = [];

    /**
     * Methods to be executed before this validate
     *
     * @var array
     */
    private $befores = [];

    /**
     * Methods to be executed after this validate
     *
     * @var array
     */
    private $afters = [];

    /**
     * This validation requires a default value for the value
     *
     * @var array
     */
    private $defaults = [];

    /**
     * Filters to be passed for this validation
     *
     * @var array
     */
    private $filters = [];

    /**
     * Error Message Provider
     *
     * @var MessageProviderInterface
     */
    private $messageProvider = null;

    /**
     * Data to be validated
     *
     * @var array
     */
    private $checkData = [];

    /**
     * Data validated this time
     *
     * @var array
     */
    private $validatedData = [];

    /**
     * Fields validated this time
     *
     * @var array
     */
    private $validateFields = [];

    /**
     * {@inheritdoc }
     */
    protected $sceneProvider = ValidateScene::class;
    
    /**
     * Create a validator
     *
     * @param array $rules               Validation rules
     * @param array $messages            Error message
     * @param array $customAttributes    Field Name
     * @return Validate
     */
    public static function make(array $rules = [], array $messages = [], array $customAttributes = []): Validate
    {
        return (new static())->setRules($rules)->setMessages($messages)->setCustomAttributes($customAttributes);
    }

    /**
     * Get Validator Factory
     *
     * @return Factory
     */
    private static function getValidationFactory(): Factory
    {
        return ValidateConfig::instance()->getFactory();
    }
    
    /**
     * Auto validate
     *
     * @param array $data Data to be verified
     * @return array
     * @throws ValidateException
     */
    public function check(array $data): array
    {
        try {
            $this->init();
            $this->checkData = $data;
            $this->addEvent('event', $this->event);
            $this->handleEvent($data, 'beforeValidate');
            $events       = $this->events;
            $this->events = [];
            $rule         = $this->getSceneRules();
            $data         = $this->pass($data, $rule);
            $this->events = $events;
            $this->handleEvent($data, 'afterValidate');
            return $data;
        } catch (ValidationException $e) {
            $error = $this->getMessageProvider()->handleMessage($e->getMessage());
            throw new ValidateException($error, 403, $e->getAttribute(), $e);
        }
    }

    /**
     * Perform data validation and processing
     *
     * @param array $data  Data to be verified
     * @param array $rules Rules for validation
     * @return array
     * @throws ValidateException
     * @throws ValidationException
     */
    private function pass(array $data, array $rules): array
    {
        $this->defaults = array_merge($this->default, $this->defaults);
        $this->filters  = array_merge($this->filter, $this->filters);

        // Validated fields are not re-validated
        $checkFields          = array_diff(array_keys($rules), $this->validateFields);
        $checkRules           = array_intersect_key($rules, array_flip($checkFields));
        $checkRules           = $this->getCheckRules($checkRules);
        $this->validateFields = array_merge($this->validateFields, $checkFields);

        if ($this->filled) {
            $checkRules = $this->addFilledRule($checkRules);
        }

        // Defaults and filters only handle the fields that are being validated now
        $fields = array_keys($checkRules);
        $data   = $this->handleDefault($data, $fields);

        if ($this->eventPriority) {
            $this->handleEvent($data, 'beforeValidate');
            $this->handleEventCallback($data, 'before');
        } else {
            $this->handleEventCallback($data, 'before');
            $this->handleEvent($data, 'beforeValidate');
        }

        $validatedData = $this->validatedData;
        if (!empty($checkRules)) {
            $validatedData = $this->getValidationFactory()->make($data, $checkRules, $this->message, $this->customAttributes)->validate();
            $validatedData = array_merge($this->validatedData, $validatedData);
        }

        if ($this->eventPriority) {
            $this->handleEventCallback($validatedData, 'after');
            $this->handleEvent($validatedData, 'afterValidate');
        } else {
            $this->handleEvent($validatedData, 'afterValidate');
            $this->handleEventCallback($validatedData, 'after');
        }

        $validatedData = $this->handlerFilter($validatedData, $fields);
        $this->initScene();
        $this->scene(null);
        return $validatedData;
    }

    /**
     * Add a custom implicit validation rule
     *
     * @param string               $rule      Rule Name
     * @param Closure|string|array $extension Closure rules, providing four parameters:$attribute, $value, $parameters, $validator
     * @param string|null          $message   Error Message
     * @return Validate
     */
    protected function extendImplicitRule(string $rule, $extension, ?string $message = null): Validate
    {
        self::validatorExtend('Implicit', $rule, $extension, $message, true);
        return $this;
    }

    /**
     * Add a custom dependent validation rule.
     *
     * @param string               $rule      Rule Name
     * @param Closure|string|array $extension Closure rules, providing four parameters:$attribute, $value, $parameters, $validator
     * @param string|null          $message   Error Message
     * @return Validate
     */
    protected function extendDependentRule(string $rule, $extension, ?string $message = null): Validate
    {
        self::validatorExtend('Dependent', $rule, $extension, $message, true);
        return $this;
    }

    /**
     * Invoke custom scenario closures for validation
     *
     * @param callable $fn
     * @param array    $data
     * @return array
     * @throws ValidateException
     */
    public function invokeSceneCheck(callable $fn, array $data): array
    {
        try {
            $this->init();
            $this->checkData = $data;
            $this->addEvent('event', $this->event);
            $this->handleEvent($data, 'beforeValidate');
            $events       = $this->events;
            $this->events = [];

            $rules = $this->invokeSceneFunction($fn, $this->rule, $this->checkData);

            $data         = $this->pass($data, $rules);
            $this->events = $events;
            $this->handleEvent($data, 'afterValidate');
            return $data;
        } catch (ValidationException $e) {
            $error = $this->getMessageProvider()->handleMessage($e->getMessage());
            throw new ValidateException($error, 403, $e->getAttribute(), $e);
        }
    }

    /**
     * Calling custom scene closures
     *
     * @param callable $fn
     * @param array    $rule
     * @param array    $data
     * @param string   $sceneName
     * @return array|mixed
     *
     * @throws ValidateException
     * @throws ValidationException
     */
    private function invokeSceneFunction(callable $fn, array $rule, array $data, string $sceneName = '')
    {
        $scene = new $this->sceneProvider($rule, $data);
        call_user_func($fn, $scene);
        $this->events        = $scene->events;
        $this->afters        = $scene->afters;
        $this->befores       = $scene->befores;
        $this->defaults      = $scene->defaults;
        $this->filters       = $scene->filters;
        $this->eventPriority = $scene->eventPriority;
        $next                = $scene->next;
        $sceneRule           = $scene->getRules();
        if (!empty($next)) {
            return $this->next($scene->next, $sceneRule, $sceneName);
        }
        return $sceneRule;
    }

    /**
     * Get the rules that need to be validation in the scene
     *
     * @param string|null $sceneName The scene name, or the current scene name if not provided.
     * @return array
     * @throws ValidationException|ValidateException
     */
    private function getSceneRules(?string $sceneName = ''): array
    {
        if ('' === $sceneName) {
            $sceneName = $this->getCurrentSceneName();
        }

        if (empty($sceneName)) {
            return $this->rule;
        }

        if (method_exists($this, 'scene' . ucfirst($sceneName))) {
            return $this->invokeSceneFunction([$this, 'scene' . ucfirst($sceneName)], $this->rule, $this->checkData, $sceneName);
        }

        if (isset($this->scene[$sceneName])) {
            $sceneRule = $this->scene[$sceneName];

            foreach (['event', 'before', 'after'] as $eventType) {
                if (isset($sceneRule[$eventType])) {
                    $callback = $sceneRule[$eventType];
                    $this->addEvent($eventType, $callback);
                    unset($sceneRule[$eventType]);
                }
            }

            if (isset($sceneRule['next']) && !empty($sceneRule['next'])) {
                $next = $sceneRule['next'];
                unset($sceneRule['next']);
                $rules = Common::getRulesAndFill($this->rule, $sceneRule);
                return $this->next($next, $rules, $sceneName);
            } else {
                return Common::getRulesAndFill($this->rule, $sceneRule);
            }
        }

        return $this->rule;
    }

    /**
     * Processing the next scene
     *
     * @param string $next             Next scene name or scene selector
     * @param array  $rules            Validation rules
     * @param string $currentSceneName Current scene name
     * @return array
     * @throws ValidateException
     * @throws ValidationException
     */
    private function next(string $next, array $rules, string $currentSceneName = ''): array
    {
        if ($next === $currentSceneName) {
            throw new ValidateRuntimeException('The scene used cannot be the same as the current scene.');
        }

        // Pre-validation
        $data                = $this->pass($this->checkData, $rules);
        $this->validatedData = array_merge($this->validatedData, $data);

        // If a scene selector exists
        if (method_exists($this, lcfirst($next) . 'Selector')) {
            $next = call_user_func([$this, lcfirst($next) . 'Selector'], $this->validatedData);
            if (is_array($next)) {
                return Common::getRulesAndFill($this->rule, $next);
            }
        }

        if (empty($next)) {
            return [];
        }
        
        return $this->getSceneRules($next);
    }

    /**
     * Processing method
     *
     * @param array $data
     * @param string $type 'before' or 'after'
     * @throws ValidateException
     */
    private function handleEventCallback(array $data, string $type)
    {
        switch ($type) {
            case 'before':
                $callbacks = $this->befores;
                break;
            case 'after':
                $callbacks = $this->afters;
                break;
        }

        if (empty($callbacks)) {
            return;
        }

        foreach ($callbacks as $callback) {
            list($callback, $param) = $callback;
            if (!is_callable($callback)) {
                $callback = $type . ucfirst($callback);
                if (!method_exists($this, $callback)) {
                    throw new ValidateRuntimeException('Method Not Found');
                }
                $callback = [$this, $callback];
            }

            if (($result = call_user_func($callback, $data, ...$param)) !== true) {
                if (isset($this->message[$result])) {
                    $result = $this->getMessageProvider()->handleMessage($this->message[$result]);
                }
                throw new ValidateException($result, 403);
            }
        }
    }

    /**
     * validate event handling
     *
     * @param array $data    Validated data
     * @param string $method Event Name
     * @throws ValidateException
     */
    private function handleEvent(array $data, string $method)
    {
        if (empty($this->events)) {
            return;
        }

        foreach ($this->events as $events) {
            list($callback, $param) = $events;
            if (class_exists($callback) && is_subclass_of($callback, ValidateEventAbstract::class)) {
                /** @var ValidateEventAbstract $handler */
                $handler            = new $callback(...$param);
                $handler->sceneName = $this->getCurrentSceneName();
                $handler->data      = $data;
                if (true !== call_user_func([$handler, $method])) {
                    $message = $handler->message;
                    if (isset($this->message[$message])) {
                        $message = $this->getMessageProvider()->handleMessage($this->message[$message]);
                    }
                    throw new ValidateException($message, 403);
                }
            } else {
                throw new ValidateRuntimeException('Event error or nonexistence');
            }
        }
    }

    /**
     * Filters for processing settings
     *
     * @param array $data
     * @param array $fields
     * @return array
     */
    private function handlerFilter(array $data, array $fields): array
    {
        if (empty($this->filters)) {
            return $data;
        }

        $newData = validate_collect($data);
        $filters = array_intersect_key($this->filters, array_flip($fields));
        foreach ($filters as $field => $callback) {
            if (null === $callback) {
                continue;
            }
            if (false !== strpos($field, '*')) {
                $flatData = ValidationData::initializeAndGatherData($field, $data);
                $pattern  = str_replace('\*', '[^\.]*', preg_quote($field));
                foreach ($flatData as $key => $value) {
                    if (Str::startsWith($key, $field) || preg_match('/^' . $pattern . '\z/', $key)) {
                        $this->filterValue($key, $callback, $newData);
                    }
                }
            } else {
                $this->filterValue($field, $callback, $newData);
            }
        }

        return $newData->toArray();
    }

    /**
     * Filter the given value
     *
     * @param string                           $field    Name of the data field to be processed
     * @param callable|Closure|FilterInterface $callback The filter. This can be a global function name, anonymous function, etc.
     * @param Collection $data
     */
    private function filterValue(string $field, $callback, Collection $data)
    {
        if (!$data->has($field)) {
            return;
        }
        $value         = $data->get($field);
        $dataAttribute = new DataAttribute();

        if (is_callable($callback)) {
            $value = call_user_func($callback, $value);
        } elseif ((is_string($callback) || is_object($callback)) && class_exists($callback) && is_subclass_of($callback, FilterInterface::class)) {
            /** @var FilterInterface $filter */
            $filter = new $callback($dataAttribute);
            $value  = $filter->handle($value);
        } elseif (is_string($callback) && method_exists($this, 'filter' . ucfirst($callback))) {
            $value = call_user_func([$this, 'filter' . ucfirst($callback)], $value, $dataAttribute);
        } else {
            throw new ValidateRuntimeException('The provided filter is wrong');
        }

        if (true === $dataAttribute->deleteField) {
            $data->forget($field);
        } else {
            $data->set($field, $value);
        }
    }

    /**
     * Defaults for processing settings
     *
     * @param array $data
     * @param array $fields
     * @return array
     */
    private function handleDefault(array $data, array $fields): array
    {
        if (empty($this->defaults)) {
            return $data;
        }

        $newData  = validate_collect($data);
        $defaults = array_intersect_key($this->defaults, array_flip($fields));
        foreach ($defaults as $field => $value) {
            // Skip array members
            if (null === $value || false !== strpos($field, '*')) {
                continue;
            }

            if (is_array($value) && isset($value['any']) && isset($value['value'])) {
                $this->setDefaultData($field, $value['value'], $newData, (bool)$value['any']);
            } else {
                $this->setDefaultData($field, $value, $newData);
            }
        }

        return $newData->toArray();
    }

    /**
     * Applying default settings to data
     *
     * @param string                                   $field    Name of the data field to be processed
     * @param callable|Closure|DefaultInterface|mixed  $callback The default value or an anonymous function that returns the default value which will
     * @param Collection                       $data     Data to be processed
     * @param bool                                     $any      Whether to handle arbitrary values, default only handle values that are not null
     */
    private function setDefaultData(string $field, $callback, Collection $data, bool $any = false)
    {
        $isEmpty = function ($value) {
            return null === $value || [] === $value || '' === $value;
        };
        $value         = $data->get($field);
        $dataAttribute = new DataAttribute();

        if ($isEmpty($value) || true === $any) {
            if (is_callable($callback)) {
                $value = call_user_func($callback, $value, $field, $this->checkData, $dataAttribute);
            } elseif ((is_string($callback) || is_object($callback)) && class_exists($callback) && is_subclass_of($callback, DefaultInterface::class)) {
                /** @var DefaultInterface $default */
                $default = new $callback($dataAttribute);
                $value   = $default->handle($value, $field, $this->checkData);
            } elseif (is_string($callback) && method_exists($this, 'default' . ucfirst($callback))) {
                $value = call_user_func([$this, 'default' . ucfirst($callback)], $value, $field, $this->checkData, $dataAttribute);
            } else {
                $value = $callback;
            }
        }
        
        if (true === $dataAttribute->deleteField) {
            $data->forget($field);
        } else {
            $data->set($field, $value);
        }
    }

    /**
     * Initialization validate
     */
    private function init()
    {
        $this->validatedData  = [];
        $this->validateFields = [];
        $this->initScene();
    }

    /**
     * Initialization validation scene
     */
    private function initScene()
    {
        $this->afters        = [];
        $this->befores       = [];
        $this->events        = [];
        $this->defaults      = [];
        $this->filters       = [];
        $this->eventPriority = true;
    }

    /**
     * Set the message provider for the validator.
     *
     * @param MessageProviderInterface|string|callable $messageProvider
     * @return $this
     * @throws ValidateException
     */
    public function setMessageProvider($messageProvider): RuleManager
    {
        if (is_string($messageProvider) && is_subclass_of($messageProvider, MessageProviderInterface::class)) {
            $this->messageProvider = new $messageProvider();
        } elseif (is_object($messageProvider) && is_subclass_of($messageProvider, MessageProviderInterface::class)) {
            $this->messageProvider = $messageProvider;
        } elseif (is_callable($messageProvider)) {
            $messageProvider = call_user_func($messageProvider);
            $this->setMessageProvider($messageProvider);
            return $this;
        } else {
            throw new ValidateRuntimeException('The provided message processor needs to implement the MessageProviderInterface interface');
        }

        return $this;
    }

    /**
     * Get the message provider for the validator.
     *
     * @return MessageProviderInterface
     */
    public function getMessageProvider(): MessageProviderInterface
    {
        if (empty($this->messageProvider)) {
            $this->messageProvider = new MessageProvider();
        }

        $messageProvider = $this->messageProvider;
        $messageProvider->setMessage($this->message);
        $messageProvider->setCustomAttributes($this->customAttributes);
        $messageProvider->setData($this->checkData);
        return $messageProvider;
    }

    /**
     * Add Event
     *
     * @param string $type 'event','before','after
     * @param string|array $callback
     */
    private function addEvent(string $type, $callback)
    {
        $type .= 's';
        $callbacks = $this->$type;

        if (is_string($callback)) {
            $callbacks[] = [$callback, []];
        } else {
            foreach ($callback as $classOrMethod => $param) {
                if (is_int($classOrMethod)) {
                    $callbacks[] = [$param, []];
                } elseif (is_string($classOrMethod)) {
                    if (is_array($param)) {
                        $callbacks[] = [$classOrMethod, $param];
                    } else {
                        $callbacks[] = [$classOrMethod, [$param]];
                    }
                }
            }
        }
        
        $this->$type = $callbacks;
    }
}
